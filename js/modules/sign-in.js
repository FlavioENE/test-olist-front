import {
  handleValidation,
  handleSubmit,
} from '../handles' 

const boxStrength = {
  charLength: document.querySelector('.box-strength .length'),
  uppercase: document.querySelector('.box-strength .uppercase'),
  number: document.querySelector('.box-strength .number')
};

export default class SignIn {
  constructor(selector) {
    this.fieldPassword = document.querySelectorAll(selector);
    this.form = document.querySelector('form').getElementsByTagName('input');
  }

  validate() {
    handleValidation(boxStrength, this.value)
  }

  addValidationEvent() {
    this.fieldPassword.forEach((item) => {
      item.addEventListener('keyup', this.validate);
    });
  }

  submit(e) {
    e.preventDefault();
    handleSubmit(e.target);
  }

  onSubmit() {
    const form = document.querySelector('form');
    form.addEventListener('submit', this.submit)
  }

  init() {
    if (this.fieldPassword.length) {
      this.addValidationEvent();
      this.onSubmit();
    }
    return this;
  }
}
