import { addClass, removeClass, hasClass } from '../utils-dom'
import { validateCharLength, validateNumber, validateUppercase } from '../validations'

export const hangleChangeStrengthGouge = (htmlCollection, className, sliceIndex) => {
  const list = Array.from(htmlCollection);
  const inputPassword = document.querySelector('#password');
  inputPassword.className = 'input-form';

  list.forEach(list => list.className = '');
  if (sliceIndex <= 0) { return null; }

  list.slice(0, sliceIndex).forEach((item) => {
    item.classList.add(className);
  });
  if (sliceIndex === 1 || sliceIndex === 2) {
    removeClass(inputPassword, 'strong-password');
    return addClass(inputPassword, 'weak-input');
  }
  removeClass(inputPassword, 'weak-input');
  return addClass(inputPassword, 'strong-password');

}

export const handleSetProgressValidate = () => {
  const list = document.querySelectorAll('.box-strength li');
  const progress = document.querySelector('.strength-gauge');
  let amount = 0;
  list.forEach((it) => {
  	amount += it.classList.contains('valid') ? 1 : 0
  });

  switch(amount) {
    case 1: 
      hangleChangeStrengthGouge(progress.children,'weak', amount);
      break
    case 2: 
      hangleChangeStrengthGouge(progress.children,'medium', amount);
      break
    case 3: 
      hangleChangeStrengthGouge(progress.children,'strong', amount);
      break
    default:
      hangleChangeStrengthGouge(progress.children, '', amount);
  }
}

export const handleValidateTest = (isValid, response, value) => {
  if (!value.length) { 
    removeClass(response, 'valid');
    removeClass(response, 'invalid');
  }

  if (isValid) { addClass(response, 'valid'); }
  else if (!isValid && value.length > 0) {
    removeClass(response, 'valid');
    addClass(response, 'invalid');
  }

  handleSetProgressValidate();
}

export const handleLoader = () => {
  const form = document.querySelector('form');
  const btn = document.getElementById('register-btn');
  const newElem = document.createElement('div');

  Array.from(form).forEach((e) => {
    if (e.tagName === 'INPUT') {
      addClass(e, 'is-valid');
    }
  })

  newElem.className = 'loader';
  for(let i=0; i < 3; i++) {
    const dot = document.createElement('div');
    dot.className= 'dot';
    newElem.appendChild(dot);
  };

  btn.replaceChild(newElem, btn.firstElementChild);
}

export const handleSuccess = () => {
  addClass(document.querySelector('.wrapperSignIn'), 'hide');
  removeClass(document.querySelector('.wrapper_register-success'), 'hide');
}

export const handleSubmit = (form) => {
  const arr = Array.from(form)
  let isValid = false;
  arr.forEach((e) => {
    e.setCustomValidity('');
    if (e.name === 'password') { 
      isValid = hasClass(e, 'strong-password');
      // if(!isValid) { e.setCustomValidity('Por favor insira sua senha conforme os parâmetros abaixo'); }
    }
    if (e.name === 'confirm_password') { 
      isValid = document.getElementById('password').value === e.value;
      // if(!isValid) { e.setCustomValidity('Por favor insira sua senha exatamente como inseriu no campo de senha'); }
    }
  })
  
  if(!isValid) { return }

  handleLoader();
  setTimeout(() => { handleSuccess() }, 5000)
}

export const handleValidation = (defaultProps, value) => {
  handleValidateTest(validateCharLength(value), defaultProps.charLength, value);
  handleValidateTest(validateUppercase(value), defaultProps.uppercase, value);
  handleValidateTest(validateNumber(value), defaultProps.number, value);
}